﻿using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.DependencyInjection;
namespace FactorySimulator
{
	public class InitializeService
	{
        ILogger<InitializeService> logger;
        IServiceProvider serviceProvider;
        public InitializeService(ILogger<InitializeService> logger , IServiceProvider serviceProvider)
        {
            this.logger = logger;
                this.serviceProvider = serviceProvider;
        }
		public void Initialize()
		{

            var client = new FactorySimulator.swaggerClient(Settings.StockApiUrl, new HttpClient());
            client.ArticleTypeAsync(new ArticleType { Name = "tissu" });
            client.ArticleTypeAsync(new ArticleType { Name = "tshirt" });
            client.ArticleTypeAsync(new ArticleType { Name = "boutton" });
            client.SegmentDefinitionAsync(new SegmentDefinition { Name = "Segment-Machine", ArticleType = "tissu", Fields = new List<String> { "MachineId" } });
            client.SegmentDefinitionAsync(new SegmentDefinition { Name = "Segment-Machine-tshirt", ArticleType = "tshirt", Fields = new List<String> { "MachineId" } });
            client.SegmentDefinitionAsync(new SegmentDefinition { Name = "Segment-Machine-button", ArticleType = "boutton", Fields = new List<String> { "MachineId" } });
            client.SegmentDefinitionAsync(new SegmentDefinition { Name = "Segment-Machine-Day", ArticleType = "tissu", Fields = new List<String> { "MachineId", "Day" } });
            client.SegmentDefinitionAsync(new SegmentDefinition { Name = "Segment-Machine-tshirt-Day", ArticleType = "tshirt", Fields = new List<String> { "MachineId", "Day" } });
            client.SegmentDefinitionAsync(new SegmentDefinition { Name = "Segment-Machine-button-Day", ArticleType = "boutton", Fields = new List<String> { "MachineId", "Day" } });

            var tshirts = new List<string>();
            var machineTShirt = serviceProvider.GetService<Machine>().Initialize("TSHIRT-01", new Dictionary<string, int> { { "bouton", 5 }, { "tissu", 2 } }, 1000, id =>
            {
                HandleProduct(id, "TSHIRT-01", "tshirt");
                tshirts.Add(id);
            });

            var machineTissu = serviceProvider.GetService<Machine>().Initialize("TISSU-01", new Dictionary<string, int> { { "fil", 10 } }, 1000, id =>
            {
                machineTShirt.AddInputProduct(id, "tissu");
                HandleProduct(id, "TISSU-01", "tissu");
            });
            var machineTissu2 = serviceProvider.GetService<Machine>().Initialize("TISSU-02", new Dictionary<string, int> { { "fil", 10 } }, 500, id =>
            {
                machineTShirt.AddInputProduct(id, "tissu");
                HandleProduct(id, "TISSU-02", "tissu");
            });
            var machineTissu3 = serviceProvider.GetService<Machine>().Initialize("TISSU-03", new Dictionary<string, int> { { "fil", 10 } }, 500, id =>
            {
                machineTShirt.AddInputProduct(id, "tissu");
                HandleProduct(id, "TISSU-03", "tissu");
            });
            var machineBoutton = serviceProvider.GetService<Machine>().Initialize("BOUTON-01", new Dictionary<string, int> { { "plastique", 5 } }, 100, id =>
            {
                machineTShirt.AddInputProduct(id, "bouton");
                HandleProduct(id, "BOUTON-01", "boutton");
            });

            machineBoutton.Start();
            machineTissu.Start();
            machineTissu2.Start();
            machineTissu3.Start();
            machineTShirt.Start();

            // livraison
            var tokenSource = new CancellationTokenSource();
            var ct = tokenSource.Token;
            var task = Task.Run(() =>
            {
                while (!ct.IsCancellationRequested)
                {
                    logger.LogInformation("New supply ...");
                    for (int i = 0; i < 100; i++) machineTissu.AddInputProduct(Guid.NewGuid().ToString(), "fil");
                    for (int i = 0; i < 100; i++) machineTissu2.AddInputProduct(Guid.NewGuid().ToString(), "fil");
                    for (int i = 0; i < 100; i++) machineTissu3.AddInputProduct(Guid.NewGuid().ToString(), "fil");
                    for (int i = 0; i < 300; i++) machineBoutton.AddInputProduct(Guid.NewGuid().ToString(), "plastique");
                    if (tshirts.Count() > 100)
                    {
                        logger.LogInformation("outstore ...");
                        var outstores = tshirts.Take(10);
                        foreach (var x in outstores)
                        {
                            tshirts.Remove(x);
                            client.Article2Async(x);
                        }
                    }

                    Thread.Sleep(5000);
                }
            }, tokenSource.Token);
            while (true)
            {
                /// To improve
                Thread.Sleep(10);
            }

            void HandleProduct(string id, string machineName, string type)
            {
                try
                {

                    client.ArticleAsync(new Article
                    {
                        ArticleType = type,
                        LogicalId = id,
                        Properties = new Dictionary<string, string>
        {
            {"MachineId",machineName},
            {"Day",DateTime.Now.ToShortDateString()},
            {"Year",DateTime.Now.Year.ToString()},
            {"Month",DateTime.Now.Month.ToString()}
        }
                    })
                        .Wait();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex.Message);
                }

            }
        }
	}
}

