using Microsoft.Extensions.Logging;

namespace FactorySimulator
{
    public class Machine
    {

        FactorySimulator.swaggerClient client = new FactorySimulator.swaggerClient(Settings.StockApiUrl, new HttpClient());
        private  int frequency;
        Dictionary<string, int> bom;
        private  string machineName;
        Action<string>? outputAction;
        ILogger<Machine> logger;
        public Machine(ILogger<Machine> logger)
        {   
            this.logger = logger;
        }
        public Machine Initialize(string machineName, Dictionary<string, int> bom, int frequency,Action<string>? outputAction)
        {
            this.bom = bom;
            this.machineName = machineName;
            this.frequency = frequency/Settings.SpeedFactor;
            this.outputAction = outputAction;
            return this;
        }
        readonly Dictionary<string, List<string>> InputProducts = new Dictionary<string, List<string>>();
        private CancellationTokenSource? tokenSource;

        public void AddInputProduct(string id, string productType)
        {
            lock(InputProducts)
            {
                var current = InputProducts.ContainsKey(productType) ? InputProducts[productType] : new List<string>();
                current.Add(id);
                InputProducts[productType] = current;
            }
        }

        public void Start()
        {
                    tokenSource = new CancellationTokenSource();
                    var ct = tokenSource.Token;

                    var task = Task.Run(() =>
                    {
                        while(!ct.IsCancellationRequested)
                        {
                            Thread.Sleep(frequency);
                            Produce();
                        }
                    }, tokenSource.Token); 


        }

        public void Stop()
        {
            tokenSource?.Cancel();
        }

        public void Produce()
        {
            lock(InputProducts)
            {
                // check product availability
                foreach (var p in bom)
                {
                    List<string> available = null;
                    InputProducts.TryGetValue(p.Key, out available);
                    if (available ==null || available.Count() < p.Value)
                    {
                        logger.LogInformation($"{machineName} - Not enough {p.Key} available...");
                        return;
                    }
                }
                // produce
                foreach (var p in bom)
                {
                    foreach(var consumed in InputProducts[p.Key].Take(p.Value))
                    {
                        InputProducts[p.Key].Remove(consumed);

                        try
                        {

                            client.Article2Async(consumed).Wait();
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex,"Article cannot be pushed");
                        }
                    }
                }
                // output
                logger.LogInformation($"{machineName} - new product");
                if(outputAction!=null) outputAction(Guid.NewGuid().ToString());  
            }
        }
    }
}