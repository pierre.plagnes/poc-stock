﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using FactorySimulator;

Console.WriteLine("Starting Factory !");

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
        services.AddSingleton<InitializeService>()
        .AddTransient<Machine>())
    .Build();

host.Services.GetService<InitializeService>().Initialize();

await host.RunAsync();

