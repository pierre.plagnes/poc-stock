﻿using System;
namespace FactorySimulator
{
	public static class Settings
    {
        public static string StockApiUrl
        {
            get
            {
                var val = Environment.GetEnvironmentVariable("STOCK_API");
                if (val != null) return val;
                return @"http://localhost:5184/";
            }
        }
        public static int SpeedFactor
        {
            get
            {
                var val = Environment.GetEnvironmentVariable("SPEED_FACTOR");
                if (val != null) return int.Parse(val);
                return 1;
            }
        }
    }
    }

