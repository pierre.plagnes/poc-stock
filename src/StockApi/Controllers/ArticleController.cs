using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prometheus;
using StockApi.Extensions;
using StockApi.Mappers;
using StockApi.Services;

namespace StockApi.Controllers;

[ApiController]
[Route("[controller]")]
public class ArticleController : ControllerBase
{

    private readonly ILogger<ArticleController> _logger;
    private readonly StockContext context;
    private readonly ISegmentService segmentService;
    private readonly MetricService metricService;

    public ArticleController(ILogger<ArticleController> logger, StockContext context, ISegmentService segmentService, MetricService metricService)
    {
        this.segmentService = segmentService;
        this.metricService = metricService;
        this.context = context;
        _logger = logger;
    }


    [HttpGet]
    public IEnumerable<Models.Article> Get()
    {
        return context.Articles
            .Include(x => x.Properties)
            .Include(x => x.ArticleType)
            .Select(x => x.ToModel());
    }


    [HttpPost]
    public async Task Post(Models.Article article)
    {

        ///TODO add article validation

        using (metricService.ArticleUpdateExecutionTimeSummary.NewTimer())
        {
            metricService.ArticleUpdateCount.Inc();

            var articleType = context.ArticleTypes.FirstOrDefault(x => article.ArticleType == x.Name);
            if (articleType == null)
            {
                throw new ArgumentException($"Unknown article type {article.ArticleType}");
            }

            var entity = context.Articles.Include(x => x.Properties).Include(x => x.SegmentValues).FirstOrDefault(x => article.LogicalId == x.LogicalId);
            if (entity == null)
            {
                entity = article.ToEntity(articleType);
                entity.LastUpdate = DateTime.Now.SetKindUtc();
                context.Articles.Add(entity);
                segmentService.ApplyNewArticle(entity);
            }
            else
            {
                entity.Merge(article);
                entity.LastUpdate = DateTime.Now.SetKindUtc();
                context.Articles.Update(entity);
                segmentService.ApplyNewArticle(entity);
            }
            context.SaveChanges();
            Ok();
        }

    }

    [HttpDelete("{logicalId}")]
    public async Task Delete(string logicalId)
    {
        using (metricService.ArticledeleExecutionTimeSummary.NewTimer())
        {
            metricService.ArticleDeleteCount.Inc();

            var article = context.Articles.Include(x => x.SegmentValues).Include(x => x.Properties).FirstOrDefault(x => x.LogicalId == logicalId);
            if (article == null)
            {
                NotFound();
            }
            else
            {
                context.Articles.Remove(article);
                segmentService.ApplyRemoveArticle(article);
                context.SaveChanges();
                Ok();
            }
        }
    }
}
