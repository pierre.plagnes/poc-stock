using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StockApi.Mappers;

namespace StockApi.Controllers;

[ApiController]
[Route("[controller]")]
public class ArticleTypeController : ControllerBase
{

    private readonly ILogger<ArticleTypeController> _logger;
    private readonly StockContext context;

    public ArticleTypeController(ILogger<ArticleTypeController> logger, StockContext context)
    {
        this.context = context;
        _logger = logger;
    }


    [HttpGet]
    public IEnumerable<Models.ArticleType> Get()
    {
        return context.ArticleTypes
            .Select(x=>x.ToModel());
    }


    [HttpPost]
    public async Task Post(Models.ArticleType articleType)
    {
        var entity = context.ArticleTypes.FirstOrDefault(x=>articleType.Name == x.Name);
        if (entity ==null)
        {
            context.ArticleTypes.Add(articleType.ToEntity());
        }
        else
        {
        }
        context.SaveChanges();
    }
}
