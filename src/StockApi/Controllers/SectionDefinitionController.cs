using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StockApi.Mappers;

namespace StockApi.Controllers;

[ApiController]
[Route("[controller]")]
public class SegmentDefinitionController : ControllerBase
{

    private readonly ILogger<ArticleTypeController> _logger;
    private readonly StockContext context;

    public SegmentDefinitionController(ILogger<ArticleTypeController> logger, StockContext context)
    {
        this.context = context;
        _logger = logger;
    }


    [HttpGet]
    public IEnumerable<Models.SegmentDefinition> Get()
    {
        return context.SegmentDefinitions.Include(x=>x.ArticleType)
            .Select(x=>x.ToModel());
    }


    [HttpPost]
    public async Task Post(Models.SegmentDefinition segmentDefinition)
    {
        ///TODO add SegmentDefinition validation
        var articleType = context.ArticleTypes.FirstOrDefault(x=>segmentDefinition.ArticleType == x.Name);
        if (articleType == null)
        {
            throw new ArgumentException($"Unknown article type {segmentDefinition.ArticleType }");
        }

        var entity = context.SegmentDefinitions.FirstOrDefault(x=>segmentDefinition.Name == x.Name);
        if (entity ==null)
        {
            context.SegmentDefinitions.Add(segmentDefinition.ToEntity(articleType));
        }
        else
        {
            ///TODO Modifiyng a segment definitions should remove existing values ? or recalculate it ?
            context.SegmentDefinitions.Update(entity.Merge(segmentDefinition));
        }
        context.SaveChanges();
    }
}
