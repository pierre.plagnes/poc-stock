using System;
using Microsoft.EntityFrameworkCore;

namespace StockApi.Entities
{
    [Index(nameof(LogicalId))]

    public class Article
    {
        public int Id { get; set; }
        public string LogicalId { get; set; }
        
        public ArticleType ArticleType { get; set; }
        public DateTime LastUpdate { get; set; }

        public ICollection<ArticleProperty> Properties {get;set;}

        public virtual ICollection<SegmentValue> SegmentValues { get; set; }
    }
}