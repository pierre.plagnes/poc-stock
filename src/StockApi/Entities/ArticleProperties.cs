namespace StockApi.Entities
{
    public class ArticleProperty
    {
        public int Id {get;set;}
        public string Name {get;set;}
        public string Value {get;set;}
    }
}