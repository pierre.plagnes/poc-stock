namespace StockApi.Entities
{
    public class ArticleType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}