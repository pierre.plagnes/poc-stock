namespace StockApi.Entities
{
    public class SegmentDefinition
    {
        public int Id {get;set;}
        public ArticleType ArticleType {get;set;}
        public string Name {get;set;}
        public string[] Fields {get;set;}
    }
}