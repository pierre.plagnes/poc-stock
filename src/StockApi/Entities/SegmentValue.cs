namespace StockApi.Entities
{
    public class SegmentValue
    {
        public int Id {get;set;}
        public SegmentDefinition SegmentDefinition {get;set;}
        public int count {get;set;}
        public string[] FieldValues { get; internal set; }

        public virtual ICollection<Article> Articles { get; set; }
    }
}