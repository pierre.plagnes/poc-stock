namespace StockApi.Mappers
{
    public static class ArticleMapper
    {   
        public static Models.Article ToModel(this Entities.Article article)
        {
            var m = new Models.Article
            {
                LogicalId=article.LogicalId,
                ArticleType=article.ArticleType.Name,
                Properties = new Dictionary<string, string>(),
            };

            foreach (var kv in article.Properties)
            {
                m.Properties[kv.Name] = kv.Value;
            }

            return m;
        }
        public static Entities.Article ToEntity(this Models.Article article, Entities.ArticleType articleType)
        {
            var e = new Entities.Article
            {
                LogicalId=article.LogicalId,
                ArticleType=articleType,
                SegmentValues  = new List<Entities.SegmentValue>(),
                Properties = new List<Entities.ArticleProperty>()
            };
            
            e.Properties = article.Properties.Select(x=>new Entities.ArticleProperty
            {
                Name = x.Key,
                Value = x.Value
            }).ToList();

            return e;
        }
        public static Entities.Article Merge(this Entities.Article article, Models.Article old)
        {
            foreach (var item in old.Properties)
            {
                var existingItem = article.Properties.FirstOrDefault(x=>x.Name == item.Key);
                if (existingItem != null)
                {
                    existingItem.Value = item.Value;
                }
                else
                {
                    article.Properties.Add(new Entities.ArticleProperty
                    {
                        Name = item.Key,
                        Value = item.Value
                    });
                }
            }

            return article;
        }
    }
}