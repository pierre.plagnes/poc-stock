namespace StockApi.Mappers
{
    public static class ArticleTypeMapper
    {   
        public static Models.ArticleType ToModel(this Entities.ArticleType type)
        {
            return new Models.ArticleType
            {
                Name=type.Name
            };
        }
        public static Entities.ArticleType ToEntity(this Models.ArticleType type)
        {
            return new Entities.ArticleType
            {
                Name=type.Name
            };
        }
    }
}