namespace StockApi.Mappers
{
    public static class SegmentDefinitionMapper
    {   
        public static Models.SegmentDefinition ToModel(this Entities.SegmentDefinition segmentDefinition)
        {
            return new Models.SegmentDefinition
            {
                Name=segmentDefinition.Name,
                ArticleType=segmentDefinition.ArticleType.Name,
                Fields=segmentDefinition.Fields
            };
        }
        public static Entities.SegmentDefinition ToEntity(this Models.SegmentDefinition segmentDefinition, Entities.ArticleType articleType)
        {
            return new Entities.SegmentDefinition
            {
                Name=segmentDefinition.Name,
                ArticleType = articleType,
                Fields=segmentDefinition.Fields
            };
        }
        public static Entities.SegmentDefinition Merge(this Entities.SegmentDefinition segmentDefinition, Models.SegmentDefinition old)
        {
            segmentDefinition.Fields=old.Fields;
            return segmentDefinition;
        }
    }
}