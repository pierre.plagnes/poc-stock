namespace StockApi.Models
{
    public class Article
    {
        public string LogicalId {get;set;}
        public string ArticleType {get;set;}
        public Dictionary<string,string> Properties {get; set;}
    }
}