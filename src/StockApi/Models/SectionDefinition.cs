namespace StockApi.Models
{
    public class SegmentDefinition
    {
        public string Name { get; set; }
        public string ArticleType {get;set;}
        public string[] Fields {get;set;}
    }
}