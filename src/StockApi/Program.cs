using StockApi;
using StockApi.Services;
using Prometheus;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<StockContext>();
builder.Services.AddScoped<ISegmentService, SegmentService>();
builder.Services.AddSingleton<MetricService>();
builder.Services.AddSingleton<Settings>();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
            
var app = builder.Build();

    app.UseSwagger();
    app.UseSwaggerUI();

//app.UseHttpsRedirection();
app.UseRouting();
app.UseHttpMetrics();
app.UseAuthorization();
app.MapControllers();
app.MapMetrics();

app.Run();
