using StockApi.Entities;

namespace StockApi.Services
{
    public interface ISegmentService
    {
        void ApplyNewArticle(Article article);
        void ApplyRemoveArticle(Article article);
    }
}