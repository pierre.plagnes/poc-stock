﻿using System;
using Prometheus;

namespace StockApi.Services
{
	public class MetricService 
	{
        public Counter ArticleUpdateCount { get; } = Metrics.CreateCounter("stock_number_of_article_update", "Number of article update request");
        public Counter ArticleDeleteCount { get; } = Metrics.CreateCounter("stock_number_of_article_delete", "Number of article delete request");
        public Histogram ArticleUpdateExecutionTimeSummary { get; } = Metrics.CreateHistogram("stock_average_execution_time_update", "update  duration.");
        public Histogram ArticledeleExecutionTimeSummary { get; } = Metrics.CreateHistogram("stock_average_execution_time_delete", "delete duration.");

        public MetricService()
		{
		}

        public void Initialize()
        {

    }
}
}

