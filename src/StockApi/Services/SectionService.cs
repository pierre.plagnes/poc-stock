using Microsoft.EntityFrameworkCore;
using StockApi.Entities;

namespace StockApi.Services
{
    public class SegmentService : ISegmentService
    {
        private readonly StockContext context;
        private readonly ILogger<SegmentService> logger;
        public SegmentService(ILogger<SegmentService> logger, StockContext context)
        {
            this.logger = logger;
            this.context = context;
        }

        public void ApplyRemoveArticle(Article article)
        {
            foreach (var sv in article.SegmentValues)
            {
                if (sv.count > 0)
                    sv.count--;
                context.SegmentValues.Update(sv);
            }
        }



        public void ApplyNewArticle(Article article)
        {
            var updated = new List<int>();

            var segmentDefinitions = context.SegmentDefinitions.Where(x => x.ArticleType.Id == article.ArticleType.Id).ToList();
            foreach (var segmentDefinition in segmentDefinitions)
            {
                bool notOk = false;
                ///TODO could be improved
                foreach (var f in segmentDefinition.Fields)
                {
                    if (!article.Properties.Any(x => x.Name == f))
                    {
                        logger.LogDebug($"Article {article.LogicalId} is not selected for segment {segmentDefinition.Name} as it does not contains field {f}");
                        notOk = true;
                        continue;
                    }
                };
                if (notOk)
                {
                    continue;
                }

                var fieldValues = new String[segmentDefinition.Fields.Length];
                for (int i = 0; i < segmentDefinition.Fields.Length; i++)
                {
                    fieldValues[i] = article.Properties.First(x => x.Name == segmentDefinition.Fields[i]).Value;
                }

                var segmentValue = context.SegmentValues.Include(x => x.Articles).FirstOrDefault(x => x.SegmentDefinition.Id == segmentDefinition.Id && x.FieldValues == fieldValues);
                if (segmentValue == null)
                {
                    segmentValue = new SegmentValue
                    {
                        count = 1,
                        SegmentDefinition = segmentDefinition,
                        FieldValues = fieldValues,
                        Articles = new List<Article>()
                    };
                    context.Add(segmentValue);
                }
                else
                {
                    if (!article.SegmentValues.Any(x => segmentValue.Id == x.Id))
                    {// Only updated if segment value is not already linked to this article
                        segmentValue.count++;
                        context.SegmentValues.Update(segmentValue);
                    }
                        updated.Add(segmentValue.Id);
                }
                segmentValue.Articles.Add(article);

                // -1 for segment that was not updated (The article is no more in it)
                var toRemove = article.SegmentValues.Where(x => !updated.Contains(x.Id)).ToList();

                foreach (var x in toRemove)
                {
                    x.count = x.count > 0 ? x.count - 1 : 0;
                    context.Update(x);
                    article.SegmentValues.Remove(x);
                }

            }
        }
    }
}