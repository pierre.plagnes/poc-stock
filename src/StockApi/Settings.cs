﻿using System;
namespace StockApi
{
    public class Settings
    {
        public string StockConnectionString
        {
            get
            {
                var val = Environment.GetEnvironmentVariable("DB_CONNECTIONSTRING");
                if (val != null) return val;
                return @"Host=localhost;Database=postgres;Username=postgres;Password=stock";
            }
        }
    }
}

