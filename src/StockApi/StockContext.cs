using Microsoft.EntityFrameworkCore;

namespace StockApi
{
    public class StockContext : DbContext
    {
        private readonly ILogger<StockContext> logger;
        private readonly IConfiguration config;
        private readonly Settings settings;

        public StockContext(ILogger<StockContext> logger, DbContextOptions options, IConfiguration config, Settings settings)
                : base(options)
        {
            this.config = config;
            this.settings = settings;
            this.logger = logger;
        }
        public DbSet<Entities.ArticleProperty> ArticleProperties { get; set; }
        public DbSet<Entities.Article> Articles { get; set; }
        public DbSet<Entities.ArticleType> ArticleTypes { get; set; }
        public DbSet<Entities.SegmentDefinition> SegmentDefinitions { get; set; }
        public DbSet<Entities.SegmentValue> SegmentValues { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseNpgsql(settings.StockConnectionString);
            //options.LogTo(Console.WriteLine);
        }

    }
}